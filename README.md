# hex-patch

## Author

allankliu@163.com

## About

hex-patch is a small Python program to patch/add/change the binary content in hex mode. You can
use it in hex file distribution, license diversity and so on.

Although it was developed for an in-house project, but it has not been used due to other
restrictions. So I put it into the public domain to share.

## Dependencies

- Python3
- intelhex (via pip3)

## Command line

```
    ("-v", "--verbose", help="")
    ("-f", "--file", help="")
    ("-a", "--address", help="")
    ("-l", "--length", help="")
    ("-b", "--base", help="")
    ("-c", "--count", help="")
    ("-i", "--iter", help="")
    ("-o", "--output", help="")
``` 

## Utilities

- create16node.sh
- rmnode.sh

These scripts are used to create more diversified hex files and remove them after using.

## License

MIT
