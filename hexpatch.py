#!/usr/bin/env python3
#coding: utf-8
import sys
import os
import struct
import getopt
import binascii
import argparse
import binascii
import random
from intelhex import IntelHex

def getOriginalHex():
    pass

def patchHex(address, val):
    pass

def dumpHex(fp):
    pass

# params: int, int, int
def createPatchHex(addr, value, bytelen):
    newIh = IntelHex()
    for i in range(bytelen):
        newIh[addr+i] = int(value / (8**i) ) & 0xFF
    return newIh

def getNewValue(base, delta, cnt):
    if delta == "inc": # increase
        return base + cnt
    if delta == "desc": # decrease
        return base - cnt
    if delta == "rand": # random
        r = int(random.random() * 0xFFFF)
        #print('random:{}'.format(r))
        res = (base + r) & 0xFFFF        
        return res
    else:
        print("Unsupported method")
        return base

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="")
    parser.add_argument("-f", "--file", help="")
    parser.add_argument("-a", "--address", help="")
    parser.add_argument("-l", "--length", help="")
    parser.add_argument("-b", "--base", help="")
    parser.add_argument("-c", "--count", help="")
    parser.add_argument("-i", "--iter", help="")
    parser.add_argument("-o", "--output", help="")

    args = parser.parse_args()
    if args.verbose:
        print("Verbose mode is ON")

    if args.file:
        fn = args.file
        print("File name:\t{}".format(fn))
    else:
        print("Please specify an intel hex file")
        sys.exit(0)

    if args.address:
        addr_raw = args.address
        t = addr_raw.split()
        c = True
        for a in t:
            if a not in ("0123456789abcdefABCDEF"):
                c = False
        if c is not True:
            addr_start = int(args.address, base=16)
            print("Start addr:\t{}".format(args.address))
        else:
            print("{} is invalid hex".format(args.address))
            sys.exit()
    else:
        print("Please specify  a hex start address")
        sys.exit()

    if args.length:
        datalen = int(args.length)
    else:
        datalen = 2
    print("Data Lenght:\t{}".format(datalen))

    if args.base:
        baseval = int(args.base,16)
    else:
        baseval = 0
    print("Base Value:\t{}".format(baseval))

    if args.count:
        count = int(args.count)
    else:
        count = 1
    print("Fanout Cnt:\t{}".format(count))

    if args.iter:
        delta = args.iter
    else:
        delta = "random"
    print("Delta Mth:\t{}".format(delta))

    #getOriginalHex(fn)
    ih = IntelHex()
    #ih.fromFile(fn, format="hex")
    #ih.LoadFile(fn, format="hex")
    ih.loadhex(fn)

    for i in range(count):
        fo = fn
        if args.output:
            fo = args.output

        f,e = fo.split(".")
        newval = getNewValue(baseval, delta, i)
        nfo = "{}_{}_{}.{}".format(f, str(i).zfill(2), hex(newval), e)
        nih = createPatchHex(addr_start, newval, datalen)
        #nih.write_hex_file(nfo)
        nfo2 = "merge_{}".format(nfo)
        ih.merge(nih, overlap="replace")
        ih.tofile(nfo2, format="hex")
        print("node-{} out:\t{}".format(str(i).zfill(2), nfo2))
    
if __name__ == "__main__":
    main()